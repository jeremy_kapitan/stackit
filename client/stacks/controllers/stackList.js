angular.module('stackit').controller('StackListCtrl', ['$scope', '$meteor',
  function($scope, $meteor) {

    // code to get popular tags
    $scope.tags = ["#gaming","#news","#technology","#people","#code"];

    $scope.$meteorSubscribe("stacks", {parentStack: {$exists: false}}).then(function() {
      $scope.stacks = $meteor.collection(Stacks);
    });

  }
]);
