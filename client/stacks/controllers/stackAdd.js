angular.module('stackit').controller('stackAddCtrl', ['$rootScope', '$scope', '$meteor', '$mdDialog', '$stateParams',
  function($rootScope, $scope, $meteor, $mdDialog, $stateParams) {

    $scope.stacks = $meteor.collection(Stacks);

    $scope.showDialog = function(ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'client/stacks/views/stackAdd.ng.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: { stacks: $scope.stacks },
        clickOutsideToClose: true
      });
    };

  }
]);

function DialogController($rootScope, $scope, $mdDialog, stacks, $stateParams) {

  $scope.stacks = stacks;
  // read state for parent stack id?
  $scope.stack = {"parentStack": $stateParams.stackId};

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.submit = function() {
    if ($scope.stackAddForm.$valid) {
      $scope.stack.user = $rootScope.currentUser._id;
      $scope.stacks.save($scope.stack);
      $mdDialog.cancel();
    } else {
      return;
    }
  };
}
