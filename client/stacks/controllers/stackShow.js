angular.module("stackit").controller("StackShowCtrl", ['$scope', '$meteor', '$stateParams',
  function($scope, $meteor, $stateParams){

    var id = $stateParams.stackId;

    // Get stack
    $scope.$meteorSubscribe('stacks', {_id: id}).then(function() {
      $scope.stack = $scope.$meteorObject(Stacks, {_id: id});
    });

    // Get substacks
    $scope.$meteorSubscribe('stacks', {parentStack: id}).then(function(handle) {
      $scope.substacks = $meteor.collection(Stacks);
    });

  }
]);
