Stacks = new Mongo.Collection("stacks");
Comments = new Mongo.Collection("comments");

Stacks.allow({
  insert: function(userId, stack) {
    return userId && stack.user === userId;
  }
});

var Schemas = {};

Schemas.Stacks = new SimpleSchema({
  title: {
    type: String,
    label: "Title",
    max: 100
  },
  url: {
    type: String,
    index: true,
    unique: true
  },
  substacks: {
    type: Schema.Stacks,
    optional: true
  },
  score: {
    type: Integer,
    defaultValue: 0
  },
  userId: { type: String },
  createdAt: { type: Date }

});

Stacks.attachSchema(Schemas.Stacks);
