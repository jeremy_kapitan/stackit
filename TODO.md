### Things to consider ###
* https://github.com/aldeed/meteor-autoform - Provides an interesting way to create forms for data.

### TODO ###
* Introduction Page to How the System works
* Convert register/login/resetpw to modals
* Add schema, null for parentStackId

### Stacks ###
* Ability to 'subscribe' to stacks for x time (1 week, 2 weeks, until canceled), default: 2 weeks
* Ability to 'subscribe' to certain tags. #gaming #science (1 week, 2 weeks, until canceled), default: until canceled
* Ability to tag stacks with keywords: "science", "news", "gaming", "nsfw" -- substacks inherit parent tags?
* redirect to home if no /s/stackid is found
* Stack's Url based on name -- guarantee uniqueness by adding numbers afterwards?
* Each Stack is Essentially it's own "Section", New Information about new or events is under the substack through activity
* Zero Reposts (Old Section with popular Stacks with new Substacks)
* Sorting Based on Views? # of Substacks? Ability to pull up or push down a stack? Combination?

### Tags ###
* autocomplete for tags
* tags for stacks
* nsfw tag hidden unless logged in and enabled

### Comments ###
* Different Comments Below Each Stack / SubStacks

### Subscriptions ###
* Ability for users to subscribe to stacks, tags, or other users

### User Page / Settings ###
* nsfw enabler
* show top 5 submitted stacks
* show top 5 stacks
* show recent stacks / comments
* single page

### Bugs ###
* Error: Could not resolve '/' from state 'register' -- Occurs when creating new account, doesn't redirect.
